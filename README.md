# Simple-LAN-Scanner3

This is my python3 port of the python2 ["Simple LAN Scanner"](https://packetstormsecurity.com/files/97353/Simple-LAN-Scanner-1.0.html) Authored by Valentin Hoebel in Jan 8, 2011

## Usage

`sudo python simple_lan_scan3.py --network=192.168.1.0/24`
